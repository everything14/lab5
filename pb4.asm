.386
.model flat, stdcall
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;includem biblioteci, si declaram ce functii vrem sa importam
includelib msvcrt.lib
extern exit: proc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;declaram simbolul start ca public - de acolo incepe executia
public start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;sectiunile programului, date, respectiv cod
.data
s1 db "are"
s1_len dd $-s1

s2 db "Ana are mere"
s2_len dd $-s2
s2_iterate dd 0

.code
start:
	xor edx, edx
	mov ebx, s2_len
	sub ebx, s1_len
	mov s2_iterate, ebx

	xor esi, esi
	;for 1
	for_s2:
		xor edi, edi
		;for 2
		for_s1:
			;compare s1[edi] s2[esi+edi]
			mov dl, [s1+edi]
			mov dh, [s2+esi+edi]
			cmp dh, dl
			jne loop_for_s2
		
			inc edi
			cmp edi, s1_len
			jl for_s1
			mov eax, esi
			jmp final
			
		loop_for_s2:
		inc esi
		cmp esi, s2_iterate
		jle for_s2

	final:
	push 0
	call exit
end start

